## Xamarin Essentials
Worked On the following..
1. Geolocation
2. Battery Status
3. Flashlight
4. SMS
5. Connectivity
6. TextToSpeech
7. Haptic feedback
8. Phone Dialer

## Results
<p>
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-essentials/-/raw/master/img1.png" width="200" height="400" />
&nbsp;
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-essentials/-/raw/master/img2.png" width="200" height="400" />
</p>
