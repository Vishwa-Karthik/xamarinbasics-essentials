﻿using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Threading;

namespace GeoLocationBasics
{
    public partial class MainPage : ContentPage
    {
        CancellationTokenSource cts;
        public MainPage()
        {
            InitializeComponent();
        }

        private async void location_services(object sender, EventArgs e)
        {
            try
            {
                Vibration.Vibrate(TimeSpan.FromMilliseconds(5));
                var request = new GeolocationRequest(GeolocationAccuracy.Best, TimeSpan.FromSeconds(10));
                cts = new CancellationTokenSource();
                var location = await Geolocation.GetLocationAsync(request, cts.Token);
                latitude.Text = location.Latitude.ToString();
                longitude.Text = location.Longitude.ToString();

            }
            catch (FeatureNotEnabledException ex)
            {
                await DisplayAlert("Warning", ex.Message, "ouch");
            }
            catch (FeatureNotSupportedException ex)
            {
                await DisplayAlert("Warning", ex.Message, "ouch");
            }
            catch (PermissionException ex)
            {
                await DisplayAlert("Warning", ex.Message, "Ouch");
            }


        }

        private async void checkNetwork(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                await DisplayAlert("Info", "Network Available", "OK");
            }
            else
            {
                await DisplayAlert("Info", "Network Unavailable", "Oouch");

            }
        }

        private async void BatteryStatus(object sender, EventArgs e)
        {
            var level = Battery.ChargeLevel;

            var state = Battery.State;

            switch (state)
            {
                case BatteryState.Charging: await DisplayAlert("Info", "Status Charging", "Ok"); break;
                case BatteryState.Full: await DisplayAlert("Info", "Charging Full", "Ok"); break;
                case BatteryState.Discharging: await DisplayAlert("Info", "Status DisCharging", "Ok"); break;
                case BatteryState.NotCharging: await DisplayAlert("Info", "charger not plugged", "Ok"); break;
                case BatteryState.NotPresent: await DisplayAlert("Info", "Battery Status Unknown", "Ok"); break;
                case BatteryState.Unknown: await DisplayAlert("Info", "Status Unavailable", "Ok"); break;
            }

        }

        private async void BatterySource(object sender, EventArgs e)
        {

            var source = Battery.PowerSource;

            switch (source)
            {
                case BatteryPowerSource.Battery: await DisplayAlert("Info", " Charging via Battery", "Ok"); break;
                case BatteryPowerSource.AC: await DisplayAlert("Info", "Charging via AC current", "Ok"); break;
                case BatteryPowerSource.Usb: await DisplayAlert("Info", " Charging via USB", "Ok"); break;
                case BatteryPowerSource.Wireless: await DisplayAlert("Info", " Charging wireless", "Ok"); break;
                case BatteryPowerSource.Unknown: await DisplayAlert("Info", "Charging source unavailable", "Ok"); break;
            }
        }

        private void AudioClicked(object sender, EventArgs e)
        {
            var btn = sender as Button;
            btn.BackgroundColor = Color.Green;
            TextToSpeech.SpeakAsync(TextController.Text);
        }

        private async void FlashLight(object sender, EventArgs e)
        {
            try
            {
                // Turn On
                await Flashlight.TurnOnAsync();

                // Turn Off
                await Flashlight.TurnOffAsync();
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                await DisplayAlert("Info", fnsEx.Message, "Ok");
            }
            catch (PermissionException pEx)
            {
                await DisplayAlert("Info", pEx.Message, "Ok");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Info", ex.Message, "Ok");
            }
        }

        private async void sendSMS(object sender, EventArgs e)
        {
            var num = Number.Text;
            var body = Body.Text;

            try
            {
                var message = new SmsMessage(body, new[] { num });
                await Sms.ComposeAsync(message);
            }
            catch (FeatureNotSupportedException ex)
            {
                await DisplayAlert("Info", ex.Message, "Ok");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Info", ex.Message, "Ok");
            }
        }

        private void CallNumber(object sender, EventArgs e)
        {
            var phnNo = PhnNumber.Text;

            try
            {
                PhoneDialer.Open(phnNo);
            }
            catch (ArgumentNullException anEx)
            {
                // Number was null or white space
            }
            catch (FeatureNotSupportedException ex)
            {
                // Phone Dialer is not supported on this device.
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }

        private async void MailMe(object sender, EventArgs e)
        {
            var btn = sender as Button;
            await btn.TranslateTo(100, 0,250);
            string mailTo = MailController.Text;
            string mailSub = MailSubController.Text;
            string mailBody = MailBodyController.Text;

            var message = new EmailMessage
            {
                Subject = mailSub,
                Body = mailBody,
                To = new List<string> { mailTo }
            };
            await Email.ComposeAsync(message);
        }
    }
}
